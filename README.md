# BUILD and INSTALL
In order to build this, the usb library must be installed.
On Ubuntu:
```
sudo apt install libusb-dev 
```
On Centos7
```
sudo yum install libusb-devel
```

Users must be in the ```dialout``` group in order to be allowed to run this.
This is done by
```
sudo addgrp myuser dialout
```

Moreover the correct udev rule must be installed and the udev must be re-triggered.
This should be done automatically by
```
make install
```


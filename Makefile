Objs                    = Antenna.o 
CC              = gcc
CXX             = g++
CCFlags         = -g -O1 -w -Wall -pedantic -fPIC -Wcpp -L/usr/lib64/ -lusb
DevFlags		=

#IncludeDirs     =  /opt/cactus/include ../

#IncludePaths            = $(IncludeDirs:%=-I%)

all: lib/libPh2_Antenna.so

%.o: %.cc %.h 
	$(CXX) -std=c++11  $(DevFlags) $(CCFlags) $(UserCCFlags) $(CCDefines) -c -o $@ $<

lib/libPh2_Antenna.so: $(Objs)
	@mkdir -p lib
	$(CC) -std=c++11 -shared -L/usr/lib64/ -lusb -o lib/libPh2_Antenna.so $(Objs) -pthread

clean:
	rm -f *.o lib/libPh2_Antenna.so

install:
	sudo cp 12-uib.rules /etc/udev/rules.d  
	sudo udevadm control --reload-rules && sudo udevadm trigger

